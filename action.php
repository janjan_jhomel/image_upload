
<?php
//action.php
if(isset($_POST["action"]))
{
 $connect = mysqli_connect("localhost", "root", "", "img_upload");
 if($_POST["action"] == "fetch")
 {
  $query = "SELECT * FROM tbl_images ORDER BY id DESC";
  $result = mysqli_query($connect, $query);
  $output = '
   <table class="styled-table">  
    <thead>
    <tr>
     <th width="40%">Image</th>
     <th width="20%">File Name</th>
     <th width="20%">Date Added</th>
     <th width="10%">Change</th>
     <th width="10%">Remove</th>
    </tr>
    </thead>
  ';
  while($row = mysqli_fetch_array($result))
  {
   $output .= '

    <tr class="active-row">
    <tbody>
     <td>
      <img src="data:image/jpeg;base64,'.base64_encode($row['name'] ).'" height="60" width="75" class="img-thumbnail" />
     </td>
     <td>
      '.$row["file_name"].'
      </td>
     <td>
      '.$row["date_added"].'
      </td>
     <td><button type="button" name="update" class="button btn-update update" id="'.$row["id"].'">Change</button></td>
     <td><button type="button" name="delete" class="button btn-delete delete" id="'.$row["id"].'">Remove</button></td>
    </tr>
    </tbody>
   ';
  }
  $output .= '</table>';
  echo $output;
 }

 if($_POST["action"] == "insert")
 {
  $file = addslashes(file_get_contents($_FILES["image"]["tmp_name"]));

  $file_name = pathinfo($_FILES['image']['name'], PATHINFO_FILENAME) . "." . pathinfo($_FILES['image']['type'], PATHINFO_FILENAME);
  $this_date = date("Y/m/d");
  $query = "INSERT INTO tbl_images(name, file_name, date_added) VALUES ('$file', '$file_name', '$this_date')";
  if(mysqli_query($connect, $query))
  {
   echo 'Image Inserted Succesfully';
  }
 }
 if($_POST["action"] == "update")
 {
  $file = addslashes(file_get_contents($_FILES["image"]["tmp_name"]));
  $file_name = pathinfo($_FILES['image']['name'], PATHINFO_FILENAME) . "." . pathinfo($_FILES['image']['type'], PATHINFO_FILENAME);
  $this_date = date("Y/m/d");
  $query = "UPDATE tbl_images SET name = '$file', file_name = '$file_name', date_added = '$this_date' WHERE id = '".$_POST["image_id"]."'";
  if(mysqli_query($connect, $query))
  {
   echo 'Image Updated into Succesfully';
  }
 }
 if($_POST["action"] == "delete")
 {
  $query = "DELETE FROM tbl_images WHERE id = '".$_POST["image_id"]."'";
  if(mysqli_query($connect, $query))
  {
   echo 'Image Deleted from Succesfully';
  }
 }
}
?>
